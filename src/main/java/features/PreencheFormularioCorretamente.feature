# language: pt
Funcionalidade: Preencher formulario de simulacao corretamente, clicar em simular e validar tabela simulacao

  Contexto: 
    Dado que estou na tela do formulario de simulacao

  Cenario: Preencher formulario de simulacao corretamente, clicar em simular e validar tabela simulacao
    Quando eu seleciono o perfil
      | perfil   |
      | juridica |
    E informo o valor que quero aplicar
      | valorAplicar |
      |         2000 |
    E informo o valor que quero poupar
      | valorPoupar |
      |        2000 |
    E informo quanto tempo quero poupar
      | tempoPoupar |
      |           5 |
    E seleciono se o tempo é em meses ou anos
      | tipoTempoPoupar |
      | anos            |
    E clico no botao simular
    Entao valido se a tabela de simulação apareceu corretamente
