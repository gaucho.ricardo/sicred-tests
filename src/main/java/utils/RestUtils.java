package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.aventstack.extentreports.Status;

public class RestUtils{

	private final static String USER_AGENT = "Chrome";

	public static String getResponseAPI(String url) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nEnviando Requisição 'GET' para URL : " + url);
		System.out.println("Código de Resposta: " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}
	
	public static boolean validaRetornoGet(String url) throws IOException {
		boolean retorno = false;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();

		if (responseCode == 200) {
			ReportUtils.logMensagem(Status.PASS, "Código de retorno: " + responseCode);
			retorno = true;
		}else {
			ReportUtils.logMensagem(Status.ERROR, "Código de retorno: " + responseCode);
		}
		return retorno;
	}

		
	public JSONObject getJsonObject(String response) throws JSONException {
		JSONObject resultFull = new JSONObject( response );
		return resultFull;
	}
	
	public JSONArray getJsonObjectArray(String response) {

		JSONArray resultFull = null;

		try {
			JSONArray jsonArray = new JSONArray(response);
			resultFull = jsonArray;

		} catch (JSONException e) {
			System.out.println("Erro: " + e);
		}

		return resultFull;
	}
	
}
