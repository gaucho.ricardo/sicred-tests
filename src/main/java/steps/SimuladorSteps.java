package steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;

import com.aventstack.extentreports.Status;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.SimuladorPage;
import utils.ReportUtils;

public class SimuladorSteps{
	
	SimuladorPage simuladorpage = new SimuladorPage();

	@Dado ("que estou na tela do formulario de simulacao")
	public void acessaTelaLoginDoPortal() {
		System.out.println("Dado que estou na tela de login do Portal.");
		simuladorpage.acessaPaginaDoSimulador();
		
	}
	
	@Quando ("eu seleciono o perfil")
    public void selecionaPerfil(DataTable credenciais) throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "Quando eu seleciono o perfil.");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.selecionaTipoPerfil(data.get(0).get("perfil"));
	}
	
	@E ("informo o valor que quero aplicar")
    public void preencheValorAplicar(DataTable credenciais) throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "E informo o valor que quero aplicar.");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.preencherValorAAplicar(data.get(0).get("valorAplicar"));
	}
	
	@E ("informo o valor que quero poupar")
    public void preencheValorPoupar(DataTable credenciais) throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "E informo o valor que quero poupar.");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.preencherValorAPoupar(data.get(0).get("valorPoupar"));
	}
	
	@E ("informo quanto tempo quero poupar")
    public void preencheTempoPoupar(DataTable credenciais) throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "E informo quanto tempo quero poupar.");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.preencherTempoAPoupar(data.get(0).get("tempoPoupar"));
	}
	
	@E ("seleciono se o tempo é em meses ou anos")
    public void preencheTipoTempoPoupar(DataTable credenciais) throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "E seleciono se o tempo é em meses ou anos.");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.selecionaTipoTempo(data.get(0).get("tipoTempoPoupar"));
	}
	
	@E ("clico no botao simular")
    public void clicaBotaoSimular() throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "E clico no botao simular.");	
		simuladorpage.clicaBotaoSimular();
	
	}
	
	@Entao ("valido se a tabela de simulação apareceu corretamente")
    public void validaTabelaSimulacao() throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "Entao valido se a tabela de simulação apareceu corretamente.");
		assertTrue("Não foi possível validar a tabela de resultado da simulação, verifique!", simuladorpage.validaTabelaResultadoSimulacao());
	}
	
	@Entao ("valido a mensagem de orientacao")
    public void validaMensagemOrientacao() throws InterruptedException, IOException {
		ReportUtils.logMensagem(Status.INFO, "Entao valido a mensagem de orientacao.");
		assertFalse("Não foi possível validar a tabela de resultado da simulação, verifique!", simuladorpage.validaTabelaResultadoSimulacao());
		assertTrue("Não foi possível validar a mensagem de orientação, verifique!", simuladorpage.validaMensagemOrientacaoValorMinimo());
	}
	
	@Dado ("que a api solicitada retorne 200")
	public void validaRespostaGetApiSimulacao(DataTable credenciais) throws JSONException, SQLException, IOException {
		ReportUtils.logMensagem(Status.INFO, "Dado que a api solicitada retorne 200");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.validaRequisicaoGet(data.get(0).get("urlApi"));
		
	}
	
	@Entao ("valido os dados e campos contidos no json")
	public void vadidaDadosMockGetApiSimulacao(DataTable credenciais) throws JSONException, SQLException, IOException, ParseException {
		ReportUtils.logMensagem(Status.INFO, "Entao valido os dados e campos contidos no json");
		List<Map<String,String>> data = credenciais.asMaps(String.class,String.class);
		simuladorpage.validaDadosApiSimulador(data.get(0).get("urlApi"));
	}

}
